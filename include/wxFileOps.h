#ifndef _WX_FILE_OPS_H_
#define _WX_FILE_OPS_H_

#include <wx/dir.h>
#include <wx/filename.h>
#include <wx/string.h>

namespace wxFileOps {

	wxULongLong GetSize(const wxString& path);
	wxULongLong GetTotalSize(const wxArrayString& paths);
	wxString HumanReadableSize(wxULongLong size);

	wxString GetName(const wxString& path);
	wxArrayString GetContents(const wxString& dir_path, int flags);

	wxString MakeRelativeTo(const wxString& path, const wxString& root);
}

#endif
