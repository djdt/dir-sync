#ifndef _DIR_FRAME_H_
#define _DIR_FRAME_H_

#include <memory>

#include <wx/button.h>
#include <wx/frame.h>
#include <wx/filepicker.h>
#include <wx/menu.h>

#include "DirTree.h"
#include "DirTreeCompare.h"

class DirFrame : public wxFrame
{
	public:
		DirFrame();
		~DirFrame();

	private:
		/* widgets */
		wxDirPickerCtrl* _dirPick_1;
		wxDirPickerCtrl* _dirPick_2;

		wxButton* _button;

		DirTree* _tree1;
		DirTree* _tree2;

		DirTreeCompare* _comp1;
		DirTreeCompare* _comp2;

		/* events */
		void OnExit (wxCommandEvent& event);
		void OnAbout(wxCommandEvent& event);

		void OnCheckHidden(wxCommandEvent& event);

		void OnPickLeft (wxFileDirPickerEvent& event);
		void OnPickRight(wxFileDirPickerEvent& event);

		void OnCompare(wxCommandEvent& event);

		void OnCopyLeft (wxCommandEvent& event);
		void OnCopyRight(wxCommandEvent& event);

		void OnDeleteLeft (wxCommandEvent& event);
		void OnDeleteRight(wxCommandEvent& event);

		wxDECLARE_EVENT_TABLE();
};

enum
{
	ID_COMPARE,
	ID_COPYLEFT,
	ID_COPYRIGHT,
	ID_DELLEFT,
	ID_DELRIGHT,
	ID_HIDDEN,
};

#endif
