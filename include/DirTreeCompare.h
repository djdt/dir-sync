#ifndef _DIR_TREE_COMPARE_H_
#define _DIR_TREE_COMPARE_H_

#include "DirTree.h"

/* Compares two trees and finds files that only exist in one (treeFrom) */

class DirTreeCompare
{
	public:
		DirTreeCompare(DirTree* treeFrom, DirTree* treeTo);

		bool CompareTrees();
		bool CopyFiles();
		bool DeleteFiles();

	private:
		DirTree* _treeFrom;
		DirTree* _treeTo;

		wxVector<wxTreeItemId> _nodes;
		wxArrayString _files;

		/* Tree Creation / Modification */
		bool CompareNode(const wxTreeItemId& node1, const wxTreeItemId& node2);
		//Returns child node of node2 that matches node1
		wxTreeItemId FindNode(const wxTreeItemId& node1, const wxTreeItemId& node2);

		//Recusive adds
		void AddAllChildren(const wxTreeItemId& node);
		void AddAllParents(const wxTreeItemId& node);

		//Node array funcs
		void HighlightFound(const wxColour& colour);

		/* File Copying */
		void CopyInit();
};

#endif
