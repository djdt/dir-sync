#ifndef _DIR_TREE_H_
#define _DIR_TREE_H_

#include <wx/imaglist.h>
#include <wx/treectrl.h>

class DirItemData : public wxTreeItemData
{
	public:
		DirItemData(const wxString& path, const wxString& name)
		:_path(path), _name(name) {}

		wxString& GetPath() { return _path; }
		wxString& GetName() { return _name; }

	private:
		wxString _path;
		wxString _name;
};

class DirTree : public wxTreeCtrl
{
	public:
		DirTree(wxWindow* parent);
		~DirTree();

		bool CreateForPath(const wxString& path);

		bool CreateSubNodes(const wxTreeItemId& node);

		bool NodeAddSubDirs(const wxTreeItemId& node);
		bool NodeAddFiles  (const wxTreeItemId& node);

		bool Refresh();

		void SetFlagHidden(bool hide = true) { _flag_hidden = hide; }

	private:
		wxImageList* _imageList;

		bool _flag_hidden;
};
#endif
