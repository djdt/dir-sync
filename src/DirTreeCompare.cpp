#include "DirTreeCompare.h"

#include <algorithm>
#include <wx/msgdlg.h>
#include <wx/progdlg.h>

#include "wxFileOps.h"

DirTreeCompare::DirTreeCompare(DirTree* treeFrom, DirTree* treeTo)
{
	_treeFrom = treeFrom;
	_treeTo = treeTo;
}

bool DirTreeCompare::CompareTrees()
{
	_nodes.clear();

	CompareNode(_treeFrom->GetRootItem(), _treeTo->GetRootItem());

	HighlightFound(wxColour(255,128,128));

	return true;
}

bool DirTreeCompare::CopyFiles()
{
	if(_nodes.size() < 1)
		return false;

	_files.clear();

	CopyInit();

	wxString rootFrom = static_cast<DirItemData*>(_treeFrom->GetItemData(_treeFrom->GetRootItem()))->GetPath();
	wxString rootTo   = static_cast<DirItemData*>(_treeTo->GetItemData(_treeTo->GetRootItem()))->GetPath();

	wxULongLong totalSize = wxFileOps::GetTotalSize(_files);
	wxULongLong copiedSize = 0;

	wxString msg;

	msg.Printf("Copy %zu files? (%s)", _files.size(),
			wxFileOps::HumanReadableSize(totalSize));

	wxMessageDialog mdlg(nullptr, "Copy Files?", msg, wxOK|wxCANCEL|wxCENTRE);

	if(mdlg.ShowModal() == wxID_CANCEL)
		return false;

	wxProgressDialog dlg("Copying", "Copying files...");

	for(wxString& file : _files)
	{
		copiedSize += wxFileOps::GetSize(file);
		wxString path = wxFileOps::MakeRelativeTo(file, rootFrom);
		path.Prepend(rootTo);
		if(wxDirExists(file))
		{
			if(!wxMkDir(path, wxS_DIR_DEFAULT))
				return false;
		}
		else
		{
			if(!wxCopyFile(file, path))
				return false;
		}
		wxULongLong fraction = totalSize - copiedSize;

		dlg.Update(100 - fraction.ToDouble() / totalSize.ToDouble() * 100.f);
	}

	HighlightFound(*wxWHITE);
	_nodes.clear();

	return true;
}

bool DirTreeCompare::DeleteFiles()
{
	if(_nodes.size() < 1)
		return false;

	_files.clear();

	CopyInit();

	wxString msg;

	msg.Printf("Delete %zu files? (%s)", _files.size(),
			wxFileOps::HumanReadableSize(wxFileOps::GetTotalSize(_files)));

	wxMessageDialog dlg(nullptr, "Delete Files?", msg, wxOK|wxCANCEL|wxCENTRE);

	if(dlg.ShowModal() == wxID_CANCEL)
		return false;

	for(wxString& file : _files)
	{
		if(wxDirExists(file))
		{
			if(!wxRmDir(file))
				return false;
		}
		else
		{
			if(!wxRemoveFile(file))
				return false;
		}
	}

	_nodes.clear();

	return true;
}

bool DirTreeCompare::CompareNode(const wxTreeItemId& node1, const wxTreeItemId& node2)
{
	bool same = true;

	wxTreeItemIdValue cookie1;
	wxTreeItemId child = _treeFrom->GetFirstChild(node1, cookie1);

	while(child.IsOk())
	{
		wxTreeItemId found = FindNode(child, node2);

		if(found == nullptr)
		{
			same = false;
			_nodes.push_back(child);
			AddAllChildren(child);
		}
		else
		{
			CompareNode(child, found);
		}

		child = _treeFrom->GetNextChild(node1, cookie1);
	}

	if(!same)
		AddAllParents(node1);

	return same;
}

wxTreeItemId DirTreeCompare::FindNode(const wxTreeItemId& node1, const wxTreeItemId& node2)
{
	wxString name1 = static_cast<DirItemData*>(_treeFrom->GetItemData(node1))->GetName();

	wxTreeItemIdValue cookie;
	wxTreeItemId child = _treeTo->GetFirstChild(node2, cookie);

	while(child.IsOk())
	{
		wxString name2 = static_cast<DirItemData*>(_treeFrom->GetItemData(child))->GetName();

		if(name1 == name2)
			return child;

		child = _treeTo->GetNextChild(node2, cookie);
	}

	return nullptr;
}

void DirTreeCompare::AddAllChildren(const wxTreeItemId& node)
{
	wxTreeItemIdValue cookie;
	wxTreeItemId child = _treeFrom->GetFirstChild(node, cookie);

	while(child.IsOk())
	{
		_nodes.push_back(child);
		if(_treeFrom->HasChildren(child))
			AddAllChildren(child);

		child = _treeFrom->GetNextChild(node, cookie);
	}
}

void DirTreeCompare::AddAllParents(const wxTreeItemId& node)
{
	wxTreeItemId parent = _treeFrom->GetItemParent(node);

	while(parent.IsOk() && parent != _treeFrom->GetRootItem())
	{
		for(wxTreeItemId& id : _nodes)
		{
			if(id != parent)
			{
				_nodes.push_back(parent);
				break;
			}
		}
		parent = _treeFrom->GetItemParent(parent);
	}
}

void DirTreeCompare::HighlightFound(const wxColour& colour)
{
	for(wxTreeItemId& node : _nodes)
	{
		_treeFrom->SetItemBackgroundColour(node, colour);
	}
}

/* Copying functions */
void DirTreeCompare::CopyInit()
{
	/* Add the node's paths to the copy file list */
	for(wxTreeItemId& node : _nodes)
	{
		_files.push_back(static_cast<DirItemData*>(_treeFrom->GetItemData(node))->GetPath());
	}

	/* Sort the list by length, makes sure directories are added bfore contents */
	std::sort(_files.begin(), _files.end());
}
