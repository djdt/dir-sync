#include "DirFrame.h"

#include "DirTreeCompare.h"

#include <wx/msgdlg.h>

wxBEGIN_EVENT_TABLE(DirFrame, wxFrame)
	EVT_MENU(wxID_EXIT,  DirFrame::OnExit)
	EVT_MENU(wxID_ABOUT, DirFrame::OnAbout)
	EVT_MENU(ID_COPYLEFT,  DirFrame::OnCopyLeft)
	EVT_MENU(ID_COPYRIGHT, DirFrame::OnCopyRight)
	EVT_MENU(ID_DELLEFT,  DirFrame::OnDeleteLeft)
	EVT_MENU(ID_DELRIGHT, DirFrame::OnDeleteRight)
	EVT_MENU(ID_HIDDEN,   DirFrame::OnCheckHidden)
	EVT_BUTTON(ID_COMPARE, DirFrame::OnCompare)
wxEND_EVENT_TABLE()

DirFrame::DirFrame()
:wxFrame(nullptr, wxID_ANY, "Dir Sync", {50,50}, {200,200})
{
	/* Menu */
	wxMenu *menuFile = new wxMenu;
	wxMenuItem* check = menuFile->AppendCheckItem(ID_HIDDEN, "&Show Hidden", "Include hidden files in trees.");
	check->Check(true);
	menuFile->Append(wxID_EXIT);

	wxMenu *menuCopy = new wxMenu;
	menuCopy->Append(ID_COPYLEFT, "&Left to Right", "Copy missing files from left tree into right.");
	menuCopy->Append(ID_COPYRIGHT, "&Right to Left", "Copy missing files from right tree into left.");

	wxMenu *menuDelete = new wxMenu;
	menuDelete->Append(ID_DELLEFT, "&Left Tree", "Delete highlighted files in left tree.");
	menuDelete->Append(ID_DELRIGHT, "&Right Tree", "Delete highlighted files in right tree.");

	wxMenu *menuHelp = new wxMenu;
	menuHelp->Append(wxID_ABOUT);

	wxMenuBar *menuBar = new wxMenuBar;
	menuBar->Append(menuFile, "&File");
	menuBar->Append(menuCopy, "&Copy");
	menuBar->Append(menuDelete, "&Delete");
	menuBar->Append(menuHelp, "&Help");
	SetMenuBar(menuBar);

	/* Status Bar */
	CreateStatusBar();
	SetStatusText("Choose two directories.");

	/* Dir Pickers */
	_dirPick_1 = new wxDirPickerCtrl(this, wxID_ANY);
	_dirPick_2 = new wxDirPickerCtrl(this, wxID_ANY);

	// pick event
	_dirPick_1->Bind(wxEVT_DIRPICKER_CHANGED, &DirFrame::OnPickLeft,  this);
	_dirPick_2->Bind(wxEVT_DIRPICKER_CHANGED, &DirFrame::OnPickRight, this);

	/* Button */
	_button = new wxButton(this, ID_COMPARE, "Compare");

	/* Dir Trees */
	_tree1 = new DirTree(this);
	_tree1->SetMinSize(wxSize(200,300));
	_tree2 = new DirTree(this);
	_tree2->SetMinSize(wxSize(200,300));

	/* Compare classes */
	_comp1 = new DirTreeCompare(_tree1, _tree2);
	_comp2 = new DirTreeCompare(_tree2, _tree1);

	/* Sizer */
	wxBoxSizer* v_sizer = new wxBoxSizer(wxVERTICAL);
	wxBoxSizer* t_sizer = new wxBoxSizer(wxHORIZONTAL);
	wxBoxSizer* h_sizer = new wxBoxSizer(wxHORIZONTAL);

	// add trees o tree sizer
	t_sizer->Add(_tree1, wxSizerFlags(1).Align(wxLEFT).Expand().Border(wxRIGHT, 5));
	t_sizer->Add(_tree2, wxSizerFlags(1).Align(wxRIGHT).Expand().Border(wxLEFT, 5));

	// add dir pickers to sizer
	h_sizer->Add(_dirPick_1, wxSizerFlags(1).Align(wxLEFT).Border(wxRIGHT, 5));
	h_sizer->Add(_button, wxSizerFlags(1).Align(wxCENTER));
	h_sizer->Add(_dirPick_2, wxSizerFlags(1).Align(wxRIGHT).Border(wxLEFT, 5));

	// add horizontal sizer to vertical
	v_sizer->Add(t_sizer, wxSizerFlags(1).Align(wxCENTER).Expand().Border(wxALL, 5));
	v_sizer->Add(h_sizer, wxSizerFlags(0).Align(wxCENTER).Expand().Border(wxALL, 5));

	SetSizerAndFit(v_sizer);
}

DirFrame::~DirFrame()
{
	wxDELETE(_dirPick_1);
	wxDELETE(_dirPick_2);

	wxDELETE(_button);

	wxDELETE(_comp1);
	wxDELETE(_comp2);

	wxDELETE(_tree1);
	wxDELETE(_tree2);
}

void DirFrame::OnExit(wxCommandEvent& event)
{
	Close();
}

void DirFrame::OnAbout(wxCommandEvent& event)
{
	wxMessageBox("Simple directory sync.", "About Dir Sync",
			wxOK | wxICON_INFORMATION);
}

void DirFrame::OnCheckHidden(wxCommandEvent& event)
{
	bool hide = event.IsChecked();

	_tree1->SetFlagHidden(hide);
	_tree2->SetFlagHidden(hide);

	if(_tree1->GetCount() > 1)
		_tree1->Refresh();
	if(_tree2->GetCount() > 1)
		_tree2->Refresh();
}

void DirFrame::OnPickLeft(wxFileDirPickerEvent& event)
{
	_tree1->CreateForPath(event.GetPath());
}

void DirFrame::OnPickRight(wxFileDirPickerEvent& event)
{
	_tree2->CreateForPath(event.GetPath());
}

void DirFrame::OnCompare(wxCommandEvent& event)
{
	if(_tree1->GetCount() > 1 && _tree2->GetCount() > 1)
	{
		_comp1->CompareTrees();
		_comp2->CompareTrees();
	}
}

void DirFrame::OnCopyLeft(wxCommandEvent& event)
{
	if(_comp1->CopyFiles())
		_tree2->Refresh();
}

void DirFrame::OnCopyRight(wxCommandEvent& event)
{
	if(_comp2->CopyFiles())
		_tree1->Refresh();
}

void DirFrame::OnDeleteLeft(wxCommandEvent& event)
{
	if(_comp1->DeleteFiles())
		_tree1->Refresh();
}

void DirFrame::OnDeleteRight(wxCommandEvent& event)
{
	if(_comp2->DeleteFiles())
		_tree2->Refresh();
}
