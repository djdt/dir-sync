// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "DirFrame.h"

class DirApp : public wxApp
{
	public:
		virtual bool OnInit();
};

wxIMPLEMENT_APP(DirApp);

bool DirApp::OnInit()
{
	DirFrame *frame = new DirFrame();
	frame->Show(true);
	return true;
};
