#include "wxFileOps.h"

wxULongLong wxFileOps::GetSize(const wxString& path)
{
	return wxFileName(path).GetSize();
}

wxULongLong wxFileOps::GetTotalSize(const wxArrayString& paths)
{
	wxULongLong size = 0;

	for(const wxString& path : paths)
	{
		size += GetSize(path);
	}
	return size;
}

wxString wxFileOps::HumanReadableSize(wxULongLong size)
{
	return wxFileName::GetHumanReadableSize(size);
}

wxString wxFileOps::GetName(const wxString& path)
{
	wxString name, ext;

	wxFileName::SplitPath(path, nullptr, &name, &ext);

	if(ext.size() > 0)
		name << "." << ext;

	return name;
}

wxArrayString wxFileOps::GetContents(const wxString& dir_path, int flags = wxDIR_FILES | wxDIR_HIDDEN)
{
	wxArrayString contents;

	if(wxDirExists(dir_path))
	{
		wxDir dir(dir_path);

		/* returns if dir not readable */
		if(!dir.IsOpened())
			return contents;

		wxString name;

		bool cont = dir.GetFirst(&name, wxEmptyString, flags);
		while(cont)
		{
			wxString path = dir.GetNameWithSep() + name;
			contents.push_back(path);
			cont = dir.GetNext(&name);
		}
	}
	else
	{
		wxPrintf("Directory \'%s\' does not exist.", dir_path);
	}
	return contents;
}

wxString wxFileOps::MakeRelativeTo(const wxString& path, const wxString& root)
{
	wxString rel = path;
	rel.Replace(root, wxEmptyString);

	return rel;
}
