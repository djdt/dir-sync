#include "DirTree.h"

#include <wx/artprov.h>
#include "wxFileOps.h"

DirTree::DirTree(wxWindow* parent)
:wxTreeCtrl(parent)
{
	// Image list for the tree.
	wxBitmap folder = wxArtProvider::GetBitmap(wxART_FOLDER, wxART_OTHER, wxSize(16,16));
	wxBitmap file = wxArtProvider::GetBitmap(wxART_NORMAL_FILE, wxART_OTHER, wxSize(16,16));
	_imageList = new wxImageList(folder.GetWidth(), folder.GetHeight(), false, 0);
	_imageList->Add(folder); //Folder, index 0
	_imageList->Add(file); //File, index 1
	SetImageList(_imageList);

	_flag_hidden = true;
}

DirTree::~DirTree()
{
	delete _imageList;
}

bool DirTree::CreateForPath(const wxString& path)
{
	if(!wxDirExists(path))
	{
		wxPrintf("\'%s\' is not a directory.", path);
		return false;
	}

	DeleteAllItems();

	wxTreeItemId root = AddRoot(path, 0, -1, new DirItemData(path, wxFileOps::GetName(path)));

	CreateSubNodes(root);

	Expand(root);

	return true;
}

/* Recursivly populates the node with all subdirs */
bool DirTree::CreateSubNodes(const wxTreeItemId& node)
{
	if(NodeAddSubDirs(node))
	{
		wxTreeItemIdValue cookie;

		wxTreeItemId child = GetFirstChild(node, cookie);

		while(child.IsOk())
		{
			CreateSubNodes(child);
			child = GetNextChild(node, cookie);
		}
	}
	return NodeAddFiles(node);
}

/* Adds sub directory nodes, non-recursive */
bool DirTree::NodeAddSubDirs(const wxTreeItemId& node)
{
	wxString path = static_cast<DirItemData*>(GetItemData(node))->GetPath();
	wxArrayString subDirs = wxFileOps::GetContents(path, _flag_hidden ? wxDIR_DIRS | wxDIR_HIDDEN : wxDIR_DIRS);
	if(subDirs.size() == 0)
		return false;

	for(wxString& dir : subDirs)
	{
		wxString name = wxFileOps::GetName(dir);
		AppendItem(node, name, 0, -1, new DirItemData(dir, name));
	}
	return true;
}

/* Adds files to node */
bool DirTree::NodeAddFiles(const wxTreeItemId& node)
{
	wxString path = static_cast<DirItemData*>(GetItemData(node))->GetPath();
	wxArrayString files = wxFileOps::GetContents(path, _flag_hidden ? wxDIR_FILES | wxDIR_HIDDEN : wxDIR_FILES);
	if(files.size() == 0)
		return false;

	for(wxString& file : files)
	{
		wxString name = wxFileOps::GetName(file);
		AppendItem(node, name, 1, -1, new DirItemData(file, name));
	}
	return true;
}

bool DirTree::Refresh()
{
	wxString root_path = static_cast<DirItemData*>(GetItemData(GetRootItem()))->GetPath();

	return CreateForPath(root_path);
}
