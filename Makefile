CC=gcc
CXX=g++
RM=rm -f
CXXFLAGS=-g -Wall -Werror -std=c++11 `wx-config --cxxflags` -O2
LDFLAGS=
LDLIBS=`wx-config --libs`
INC=-I include

SRC=src
BUILD=build
BIN=bin

TARGET=$(BIN)/dir-comp
SRCS=$(shell find $(SRC) -type f -name *.cpp)
OBJS=$(patsubst $(SRC)/%,$(BUILD)/%,$(SRCS:.cpp=.o))

$(TARGET): $(OBJS)
		@mkdir -p $(BIN)
		$(CXX) $(LDFLAGS) $^ -o $(TARGET) $(LDLIBS)

$(BUILD)/%.o: $(SRC)/%.cpp
		@mkdir -p $(BUILD)
		$(CXX) $(CXXFLAGS) $(INC) -c -o $@ $<

clean:
		$(RM) $(OBJS)
		@rmdir $(BUILD)

dist-clean: clean
		$(RM) $(TARGET)
		@rmdir $(BIN)
